//khai báo thư viện mongoose
const mongoose = require("mongoose");
//khai báo schema từ thư viện
const Schema = mongoose.Schema;
//tạo đối tượng Schema với các thuộc tính yêu cầu
const reviewSchema = new Schema({
    _id : mongoose.Types.ObjectId,

    // _id :{
    //    type: mongoose.Types.ObjectId,
    // },

    stars : {
        type: Number,
        default: 0
    },

    note : {
        type : String,
        required : false,
    },

    create_at : {
        type: Date,
        default : Date.now()
    }
});
//export Schema ra Model có tên "review"
module.exports = mongoose.model("review", reviewSchema);



