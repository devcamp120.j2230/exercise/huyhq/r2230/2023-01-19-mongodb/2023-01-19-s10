const express = require("express");

const {
    getAllCourses,
    getACourse,
    postACourse,
    putACourse,
    deleteACourse,
} = require('../middlewares/courseMiddleware');

const courseRouter = express.Router();

courseRouter.get('/course', getAllCourses, (req,res)=>{
    res.status(200).json({
        course: "Get All Course",
    })
})

courseRouter.post('/course/:id', postACourse, (req,res)=>{
    var id = req.params.id;
    res.status(200).json({
        course: "Create Course id="+id,
    })
})

module.exports = {courseRouter};