const express = require("express");

//khai báo thư viện mongoDb
const mongoose = require("mongoose");

const reviewModel = require("./app/model/reviewModel");
const courseModel = require("./app/model/courseModel");

const { courseRouter } = require("./routes/courseRoute");

const app = express();

const post = 8000;

//connect tới table mongoDb
const nameDb = "CRUD_Course";
mongoose.connect("mongodb://127.0.0.1:27017/" + nameDb, function (error) {
    if (error) throw error;
    console.log('Successfully connected to DB: '+nameDb);
})


app.use(
    (req, res, next) => {
        console.log("Date: " + new Date());
        next();
    },
    (req, res, next) => {
        console.log(req.method);
        next();
    }
)

app.get("/", (req, res) => {
    res.status(200).json({
        name: "Hello Huy",
    })
})

app.use("/", courseRouter);

app.listen(post, () => {
    console.log("Run App on port: " + post);
})